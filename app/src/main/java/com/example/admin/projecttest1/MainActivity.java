package com.example.admin.projecttest1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.admin.projecttest1.Adapter.AdapterLV1;
import com.example.admin.projecttest1.Model.ItemsRow;
import com.google.gson.Gson;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button btnnext;
    ArrayList<ItemsRow> arrWho;
    AdapterLV1 adapterLV1;
    ListView lv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity1);
        anhxa();
        arrWho = new ArrayList<>();
        arrWho.add(new ItemsRow(0,"Đỗ Ngọc Hoàn","Thanh Hoa ",20,1));
        addArraylist();


        adapterLV1 = new AdapterLV1(arrWho,getApplicationContext());
        lv1.setAdapter(adapterLV1);
        adapterLV1.notifyDataSetChanged();


    }



    @SuppressLint("WrongViewCast")
    private void anhxa() {
        btnnext = findViewById(R.id.next1);
        lv1 = findViewById(R.id.lvActivity1);
        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Activity2.class);

                Gson gson = new Gson();
                String jsonlist = gson.toJson(arrWho);
                intent.putExtra("list_as_string", jsonlist);
                startActivity(intent);
            }
        });
    }

    private void addArraylist() {

        for(int i=0;i<=100;i++) {
              arrWho.add(new ItemsRow(i,"hoan "+i,"Thanh Hoa "+i,i,1));
        }

    }
}
