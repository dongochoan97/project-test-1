package com.example.admin.projecttest1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.lang.reflect.Type;
import com.example.admin.projecttest1.Adapter.Adapter2;
import com.example.admin.projecttest1.Adapter.AdapterLV1;
import com.example.admin.projecttest1.Model.ItemsRow;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class Activity2 extends AppCompatActivity {
    ArrayList<ItemsRow> arrnew;
    Adapter2 adapter2;
    ListView lv2;
    int sl;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        arrnew= new ArrayList<>();


        String carListAsString = getIntent().getStringExtra("list_as_string");

        Gson gson = new Gson();
        Type type = new TypeToken<List<ItemsRow>>(){}.getType();

        ArrayList<ItemsRow> arr = gson.fromJson(carListAsString,type);

        for (int i=0;i<arr.size();i++)
        {
            if(arr.get(i).getTrangthai()==0)
            {
                arrnew.add(arr.get(i));
            }
        }


        lv2= findViewById(R.id.lvActivity2);

        adapter2 = new Adapter2(arrnew,getApplicationContext());
        lv2.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();

//        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(getApplicationContext(),Activity3.class);
//                intent.putExtra("keyten",arrnew.get(position).getTen());
//                intent.putExtra("keytuoi",arrnew.get(position).getTuoi());
//                intent.putExtra("keydiachi",arrnew.get(position).getDiaChi());
//                startActivity(intent);
//            }
//        });


    }
}
