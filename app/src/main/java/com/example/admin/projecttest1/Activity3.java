package com.example.admin.projecttest1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Activity3 extends AppCompatActivity {
    TextView ten, tuoi, diachi;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity3);
        ten= findViewById(R.id.ten);
        tuoi= findViewById(R.id.tuoi);
        diachi = findViewById(R.id.diachi);
        Intent intent = getIntent();
        ten.setText("Tên :"+intent.getStringExtra("keyten"));
        tuoi.setText("Tuổi :"+intent.getStringExtra("keytuoi"));
        diachi.setText("Địa chỉ :"+intent.getStringExtra("keydiachi"));

    }
}
