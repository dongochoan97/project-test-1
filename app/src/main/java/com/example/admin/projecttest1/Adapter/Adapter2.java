package com.example.admin.projecttest1.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.admin.projecttest1.Activity3;
import com.example.admin.projecttest1.Model.ItemsRow;
import com.example.admin.projecttest1.R;

import java.util.ArrayList;

public class Adapter2 extends BaseAdapter {
    ArrayList<ItemsRow> arrItem;
    Context context;

    public Adapter2(ArrayList<ItemsRow> arrItem, Context context) {
        this.arrItem = arrItem;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrItem.size();
    }

    @Override
    public Object getItem(int position) {
        return arrItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrItem.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        ViewHolder1 viewHolder;
        if(convertView==null){
            viewHolder= new ViewHolder1();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row,parent,false);
            viewHolder.txtname = convertView.findViewById(R.id.txtname);
            viewHolder.ckb = convertView.findViewById(R.id.checkbox);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder1) convertView.getTag();
        }
        final ItemsRow itemsRow = arrItem.get(position);

        int tt= itemsRow.getTrangthai();
        if(tt==0){

            viewHolder.txtname.setText(itemsRow.getTen());
        }

        viewHolder.ckb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Activity3.class);
                intent.putExtra("keyten",itemsRow.getTen());
                intent.putExtra("keytuoi",itemsRow.getTuoi());
                intent.putExtra("keydiachi",itemsRow.getDiaChi());
                parent.getContext().startActivity(intent);
            }
        });


        return convertView;
    }

    class ViewHolder1{
        TextView txtname;
        CheckBox ckb;
    }

}
