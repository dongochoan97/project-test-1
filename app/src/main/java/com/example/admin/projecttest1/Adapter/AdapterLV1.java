package com.example.admin.projecttest1.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.admin.projecttest1.Model.ItemsRow;
import com.example.admin.projecttest1.R;

import java.util.ArrayList;

public class AdapterLV1 extends BaseAdapter {
    ArrayList<ItemsRow> arrItem;
    Context context;

    public AdapterLV1(ArrayList<ItemsRow> arrItem, Context context) {
        this.arrItem = arrItem;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrItem.size();
    }

    @Override
    public Object getItem(int position) {
        return arrItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return arrItem.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView==null){
            viewHolder= new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row,parent,false);
            viewHolder.txtname = convertView.findViewById(R.id.txtname);
            viewHolder.ckb = convertView.findViewById(R.id.checkbox);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final ItemsRow itemsRow = arrItem.get(position);
        viewHolder.txtname.setText(itemsRow.getTen());
        int tt= itemsRow.getTrangthai();
        if(tt==0){
            viewHolder.ckb.setChecked(true);
        }else {
            viewHolder.ckb.setChecked(false);
        }
        viewHolder.ckb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if(isChecked){
                   itemsRow.setTrangthai(0);
                   Log.e("Check", "onCheckedChanged: 0" );
               }else {
                   itemsRow.setTrangthai(1);
                   Log.e("Check", "onCheckedChanged: 1" );
               }
            }
        });

        return convertView;
    }


    class ViewHolder{
        TextView txtname;
        CheckBox ckb;
    }
}
