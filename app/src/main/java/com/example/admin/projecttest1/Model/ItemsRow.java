package com.example.admin.projecttest1.Model;

public class ItemsRow {
    int id;
    String ten;
    String diaChi;
    int tuoi;
    int trangthai;

    public ItemsRow(int id, String ten, String diaChi, int tuoi, int trangthai) {
        this.id = id;
        this.ten = ten;
        this.diaChi = diaChi;
        this.tuoi = tuoi;
        this.trangthai = trangthai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public int getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(int trangthai) {
        this.trangthai = trangthai;
    }
}
